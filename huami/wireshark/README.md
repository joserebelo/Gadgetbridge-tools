# Huami dissector

## Devices tested

- Mi Band 5 (fw 1.0.2.66)

## Known issues

- Chunks split across multiple packets not supported
- Encryption not supported (eg. Mi Band 6)
