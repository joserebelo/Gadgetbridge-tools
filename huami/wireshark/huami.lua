-- Copyright (C) 2022 José Rebelo
--
-- This file is part of Gadgetbridge-tools.
--
-- Gadgetbridge is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Gadgetbridge is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

mi_proto = Proto("Huami",  "Huami Protocol")

mi_proto.fields.endpoint = ProtoField.uint8("mi.endpoint", "Endpoint", base.HEX)

--
-- Utils
--

function on_error(subtree, message)
    subtree:add_expert_info(PI_DEBUG, PI_ERROR, message)
end

function on_warn(subtree, message)
    subtree:add_expert_info(PI_DEBUG, PI_WARN, message)
end

function expected_length(subtree, payload, length)
    if payload:len() ~= length then
        on_error(subtree, "Unexpected payload length " .. payload:len())
    end
end

function expected_value(subtree, payload, idx, value)
    if payload(idx, 1):uint() ~= value then
        on_error(subtree, "Expected " .. value .. " at index " .. idx)
    end
end

function expected_zero(subtree, payload, idx)
    expected_value(subtree, payload, idx, 0)
end

function to_bool(num, subtree)
    if num == 0 then
        return "false"
    elseif num == 1 then
        return "true"
    else
        on_error(subtree, "Unknown value " .. num .. " for Boolean")
        return "unknown (" .. num .. ")"
    end
end

function parse_handle_noop(payload, pinfo, subtree)
    on_warn(subtree, "Noop")
end

--
-- Heart Rate Monitoring
--

function parse_heart_rate_alert(payload, subtree)
    expected_length(subtree, payload, 5)
    expected_zero(subtree, payload, 2)

    if payload(3, 1):uint() == 0 then
        subtree:add(payload(3, 1), "Heart Rate Alert: off")
    elseif payload(3, 1):uint() == 1 then
        -- 100 to 150, steps of 5
        subtree:add(payload(3, 1), "Heart Rate Alert: on")
        subtree:add(payload(4, 1), "Threshold: " .. payload(4, 1):uint() .. "bpm")
    else
        on_error(subtree, "Unexpected heart rate alert mode " .. payload(3, 1):uint())
    end
end

--
-- Configuration
--

function parse_night_mode(payload, pinfo, subtree)
    if payload(1, 1):uint() == 0 then
        subtree:add(payload(1, 1), "Night Mode = off")
        expected_length(subtree, payload, 2)
    elseif payload(1, 1):uint() == 2 then
        subtree:add(payload(1, 1), "Night Mode = after sunset")
        expected_length(subtree, payload, 2)
    elseif payload(1, 1):uint() == 1 then
        local schd_start_h = string.format("%02d", payload(2, 1):uint())
        local schd_start_m = string.format("%02d", payload(3, 1):uint())
        local schd_end_h = string.format("%02d", payload(4, 1):uint())
        local schd_end_m = string.format("%02d", payload(5, 1):uint())
        subtree:add(payload(1, 1), "Night Mode = scheduled")
        subtree:add(payload(2, 2), "Start: " .. schd_start_h .. ":" .. schd_start_m)
        subtree:add(payload(4, 2), "End: " .. schd_end_h .. ":" .. schd_end_m)
    else
        subtree:add(payload(1, 1), "Night Mode = unknown")
        subtree:add_expert_info(PI_DEBUG, PI_WARN, "Unexpected night mode mode " .. payload(1, 1):uint())
    end
end

function parse_inactivity_warnings(payload, pinfo, subtree)
    expected_length(subtree, payload, 12)
    if payload(1, 1):uint() == 0 then
        subtree:add(payload(1, 1), "Inactivity Warnings = off")
    elseif payload(1, 1):uint() == 1 then
        local schd_start_h = string.format("%02d", payload(4, 1):uint())
        local schd_start_m = string.format("%02d", payload(5, 1):uint())
        local schd_end_h = string.format("%02d", payload(6, 1):uint())
        local schd_end_m = string.format("%02d", payload(7, 1):uint())
        subtree:add(payload(1, 1), "Inactivity Warnings = on")
        subtree:add(payload(4, 2), "Interval 1 " .. schd_start_h .. ":" .. schd_start_m .. " to " .. schd_end_h .. ":" .. schd_end_m)

        if payload(8, 4):uint() ~= 0 then
            local schd_start_h = string.format("%02d", payload(8, 1):uint())
            local schd_start_m = string.format("%02d", payload(9, 1):uint())
            local schd_end_h = string.format("%02d", payload(10, 1):uint())
            local schd_end_m = string.format("%02d", payload(11, 1):uint())
            subtree:add(payload(4, 2), "Interval 2 " .. schd_start_h .. ":" .. schd_start_m .. " to " .. schd_end_h .. ":" .. schd_end_m)
        end
    else
        subtree:add(payload(1, 1), "Inactivity Warnings = unknown")
        subtree:add_expert_info(PI_DEBUG, PI_WARN, "Unexpected Inactivity Warnings mode " .. payload(1, 1):uint())
    end
end

function parse_lift_wrist(payload, subtree)
    expected_length(subtree, payload, 8)
    expected_zero(subtree, payload, 2)

    if payload(3, 1):uint() == 0 then
        subtree:add(payload(3, 1), "Lift On Wrist = off")
        for i = 4, 7 do expected_zero(subtree, payload, i) end
    elseif payload(3, 1):uint() == 1 then
        if payload(4, 4):uint() == 0 then
            subtree:add(payload(3, 1), "Lift On Wrist = on")
            for i = 4, 7 do expected_zero(subtree, payload, i) end
        else
            local schd_start_h = string.format("%02d", payload(4, 1):uint())
            local schd_start_m = string.format("%02d", payload(5, 1):uint())
            local schd_end_h = string.format("%02d", payload(6, 1):uint())
            local schd_end_m = string.format("%02d", payload(7, 1):uint())
            subtree:add(payload(3, 1), "Lift On Wrist = scheduled")
            subtree:add(payload(4, 2), "Start: " .. schd_start_h .. ":" .. schd_start_m)
            subtree:add(payload(6, 2), "End: " .. schd_end_h .. ":" .. schd_end_m)
        end
    else
        subtree:add(payload(3, 1), "Lift On Wrist = unknown")
        subtree:add_expert_info(PI_DEBUG, PI_WARN, "Unexpected lift wrist " .. payload(3, 1):uint())
    end
end

function parse_lift_wrist_response_speed(payload, subtree)
    expected_length(subtree, payload, 4)
    expected_zero(subtree, payload, 2)

    if payload(3, 1):uint() == 0 then
        subtree:add(payload(3, 1), "Lift On Wrist Response = normal")
    elseif payload(3, 1):uint() == 1 then
        subtree:add(payload(3, 1), "Lift On Wrist Response = sensitive")
    else
        subtree:add(payload(3, 1), "Lift On Wrist Response = unknown")
        on_warn(subtree, "Unexpected lift wrist response speed")
    end
end

function parse_activity_monitoring(payload, subtree)
    expected_length(subtree, payload, 4)
    expected_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Activity Monitoring Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
end

function parse_silent_mode(payload, subtree)
    expected_length(subtree, payload, 4)
    expected_zero(subtree, payload, 2)
    subtree:add(payload(3, 1), "Silent Mode: " .. to_bool(payload(3, 1):uint(), subtree))
end

display_commands = {
    --[0x0a] = { "DATEFORMAT" },
    --[0x02] = { "DATEFORMAT_12H_24H" },
    --[0x1f] = { "HR_CONNECTION" },
    --[0x01] = { "BT_CONNECTED_ADV" },
    --[0x06] = { "GOAL_NOTIFICATION" },
    --[0x0d] = { "ROTATE_WRIST_TO_SWITCH_INFO" },
    --[0x10] = { "DISPLAY_CALLER" },
    [0x22] = { "ACTIVITY_MONITORING", parse_activity_monitoring },
    [0x1a] = { "HEART_RATE_ALERT", parse_heart_rate_alert },
    [0x05] = { "LIFT_WRIST", parse_lift_wrist },
    [0x19] = { "SILENT_MODE", parse_silent_mode },
    [0x23] = { "LIFT_WRIST_RESPONSE_SPEED", parse_lift_wrist_response_speed },
}

function parse_display(payload, pinfo, subtree)
    local payload_type_code = payload(1, 1):uint()

    if display_commands[payload_type_code] == nil then
        pinfo.cols.info = "Unknown Display Command"

        on_warn(subtree, "Unknown display command " .. payload_type_code)
        return
    end

    local payload_type = "Unknown"
    payload_type = display_commands[payload_type_code][1]
    payload_handler = display_commands[payload_type_code][2]

    pinfo.cols.info = payload_type

    if (type(payload_handler) == "table") then
        -- There's a subtype
        local payload_subtype_code = payload(1,1):uint()

        if payload_handler[payload_subtype_code] == nil then
            -- Unknown payload subtype
            pinfo.cols.info:append(", unknown subtype")

            on_warn(subtree, "Unknown display command subtype " .. payload_subtype_code)
            return
        end

        payload_subtype = payload_handler[payload_subtype_code][1]
        payload_handler = payload_handler[payload_subtype_code][2]

        pinfo.cols.info:append(", " .. payload_subtype)
    end

    if (type(payload_handler) == "function") then
        payload_handler(payload, subtree)
    else
        on_warn(subtree, "Unhandled payload")
    end
end

function parse_handle_response(payload, pinfo, subtree)
    local configuration_endpoint = payload(1, 1):uint()
    local payload_type_code = payload(2, 1):uint()

    if configuration_endpoint == 0x06 then
        expected_length(subtree, payload, 5)
        expected_zero(subtree, payload, 3)

        payload_type = display_commands[payload_type_code][1]

        if payload_type ~= nil then
            subtree:add(payload(2, 1), "Type: " .. payload_type)
            subtree:add(payload(4, 1), "Success: " .. to_bool(payload(4, 1):uint(), subtree))
            pinfo.cols.info = 'ACK'
            if payload(2, 1):uint() ~= 1 then
                on_warn(subtree, "Not success")
            end
        else
            on_warn(subtree, "Unknown payload type for " .. payload_type_code)
        end
    elseif configuration_endpoint == 0x1a then
        expected_length(subtree, payload, 3)
        subtree:add(payload(2, 1), "Type: Night Mode")
        subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        pinfo.cols.info = 'ACK'
        if payload(2, 1):uint() ~= 1 then
            on_warn(subtree, "Not success")
        end
    elseif configuration_endpoint == 0x08 then
        expected_length(subtree, payload, 3)
        subtree:add(payload(2, 1), "Type: Inactivity Warnings")
        subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
        pinfo.cols.info = 'ACK'
        if payload(2, 1):uint() ~= 1 then
            on_warn(subtree, "Not success")
        end
    else
        on_warn(subtree, "Unhandled response " .. configuration_endpoint)
    end
end

function parse_handle_configuration(payload, pinfo, subtree)
    local configuration_endpoint = payload(0, 1):uint()

    if configuration_endpoint == 0x06 then
        pinfo.cols.info = 'CONFIGURATION'
        parse_display(payload, pinfo, subtree)
    elseif configuration_endpoint == 0x1a then
        pinfo.cols.info = 'NIGHT_MODE'
        parse_night_mode(payload, pinfo, subtree)
    elseif configuration_endpoint == 0x08 then
        pinfo.cols.info = 'INACTIVITY_WARNINGS'
        parse_inactivity_warnings(payload, pinfo, subtree)
    -- elseif configuration_endpoint == 0xff then
    -- Value: ff11000000 request workout types
    elseif configuration_endpoint == 0xfe then
        pinfo.cols.info = 'STRESS_MONITORING'
        subtree:add(payload(3, 1), "Enabled: " .. to_bool(payload(3, 1):uint(), subtree))
        on_warn(subtree, "Stress monitoring is WIP")
    elseif configuration_endpoint == 0x10 then
        pinfo.cols.info = 'RESPONSE'
        parse_handle_response(payload, pinfo, subtree)
    else
        on_warn(subtree, "Unhandled configuration " .. configuration_endpoint)
    end
end

--
-- Chunked
--

function parse_phone_volume(payload, subtree)
    expected_length(subtree, payload, 5)

    subtree:add(payload(4, 1), "Volume: " .. payload(4, 1):uint() .. "%")
end

function parse_vibration(payload, subtree)
    notification_types = {
        [0x00] = "APP_ALERTS",
        [0x01] = "INCOMING_CALL",
        [0x02] = "INCOMING_SMS",
        [0x04] = "GOAL_NOTIFICATION",
        [0x05] = "ALARM",
        [0x06] = "IDLE_ALERTS",
        [0x08] = "EVENT_REMINDER",
        [0x09] = "FIND_BAND";
    }

    subtree:add(payload(4, 1), "Notification Type: " .. notification_types[payload(4, 1):uint()])

    if bit32.band(payload(5, 1):uint(), 0x80) > 0 then
        subtree:add(payload(4, 1), "Test: true")
    else
        subtree:add(payload(4, 1), "Test: false")
    end

    numOnOff = bit32.band(payload(5, 1):uint(), bit32.bnot(0x80), bit32.bnot(0x40))
    subtree:add(payload(5, 1), "Num On Off: " .. numOnOff)

    expected_length(subtree, payload, numOnOff * 4 + 6)

    local subtree_pattern = subtree:add(mi_proto, buffer, "Vibration Pattern")

    local i = 6
    while i < payload:len() - 1 do
        subtree_pattern:add(payload(i, 2), "On: " .. payload(i, 2):le_uint() .. "ms")
        subtree_pattern:add(payload(i + 2, 2), "Off: " .. payload(i + 2, 2):le_uint() .. "ms")

        i = i + 4
    end
end

function parse_display_items(payload, subtree)
    menu_types = {
        [0xfd] = "Shortcut",
        [0xff] = "MenuItem",
    }

    -- from Gadgetbridge HuamiMenuType.java
    screens = {
        [0x01] = "status",
        [0x02] = "hr",
        [0x03] = "workout",
        [0x04] = "weather",
        [0x06] = "notifications",
        [0x07] = "more",
        [0x08] = "dnd",
        [0x09] = "alarm",
        [0x0a] = "takephoto",
        [0x0b] = "music",
        [0x0c] = "stopwatch",
        [0x0d] = "timer",
        [0x0e] = "findphone",
        [0x0f] = "mutephone",
        [0x10] = "nfc",
        [0x11] = "alipay",
        [0x12] = "watchface",
        [0x13] = "settings",
        [0x14] = "activity",
        [0x15] = "eventreminder",
        [0x16] = "compass",
        [0x19] = "pai",
        [0x1a] = "worldclock",
        [0x1b] = "timer_stopwatch",
        [0x1c] = "stress",
        [0x1d] = "period",
        [0x21] = "goal",
        [0x23] = "sleep",
        [0x24] = "spo2",
        [0x26] = "events",
        [0x28] = "widgets",
        [0x33] = "breathing",
        [0x34] = "steps",
        [0x35] = "distance",
        [0x36] = "calories",
        [0x38] = "pomodoro",
        [0x39] = "alexa",
        [0x3a] = "battery",
        [0x40] = "temperature",
        [0x41] = "barometer",
        [0x43] = "flashlight",
    }

    local i = 4
    while i < payload:len() - 1 do
        -- TODO: Gadgetbridge only sends enabled screens/shortcuts

        local state_str = " (unknown)"
        if payload(i + 1, 1):uint() == 0 then
            state_str = " (enabled)"
        elseif payload(i + 1, 1):uint() == 1 then
            state_str = " (disabled)"
        else
            on_error(subtree, "Unknown screen state " .. payload(i + 1, 1):uint())
        end

        local idx = payload(i, 1):uint()
        local menu_type = menu_types[payload(i + 2, 1):uint()]
        local screen_id = payload(i + 3, 1):uint()

        subtree:add(payload(i, 4), menu_type .. "[" .. idx .. "] = " .. screens[screen_id] .. state_str)

        i = i + 4
    end
end

function parse_workout_types(payload, subtree)
    expected_zero(subtree, payload, 4)

    workout_types = {
        [0x01] = "Outdoor running",
        [0x06] = "Walking",
        [0x08] = "Treadmill",
        [0x09] = "Outdoor cycling",
        [0x0a] = "Indoor cycling",
        [0x0c] = "Elliptical",
        [0x0e] = "Pool swimming",
        [0x10] = "Freestyle",
        [0x15] = "Jump rope",
        [0x17] = "Rowing machine",
        [0x3c] = "Yoga",
    }

    local i = 5
    while i < payload:len() - 1 do
        expected_zero(subtree, payload, i + 1)

        local state_str = " (unknown)"
        if payload(i + 2, 1):uint() == 0 then
            state_str = " (disabled)"
        elseif payload(i + 2, 1):uint() == 1 then
            state_str = " (enabled)"
        else
            on_error(subtree, "Unknown workout type screen state " .. payload(i + 2, 1):uint())
        end

        subtree:add(payload(i, 3), "Workout type: " .. workout_types[payload(i, 1):uint()] .. state_str)

        i = i + 3
    end
end

function parse_gps_location(payload, subtree)
    local flags = payload(0, 4):le_int()

    local flags_txt = ""

    for i = 0, 32 do
        if bit32.band(flags, 2^i) > 0 then
            flags_txt = flags_txt .. " " .. string.format("0x%x", 2^i)

            if i ~= 0 and i ~= 18 then
                -- TODO: We currently only handle 0x01 and 0x40000, but there are more (eg. if we start the workout from the phone instead of the band, the payloads contain much more data)
                on_warn(subtree, "Unknown gps location flag: " .. string.format("0x%x", 2^i))
            end
        end
    end

    subtree:add(payload(0, 4), "Flags:" .. flags_txt)

    local i = 4

    if bit32.band(flags, 0x01) > 0 then
        local gps_state = payload(i, 1):le_int()

        if gps_state == 0x02 then
            subtree:add(payload(i, 1), "GPS State: searching")
        elseif gps_state == 0x01 then
            subtree:add(payload(i, 1), "GPS State: acquired")
        elseif gps_state == 0x04 then
            subtree:add(payload(i, 1), "GPS State: disabled")
        else
            subtree:add(payload(i, 1), "GPS State: unknown")
            on_error(subtree, "Unknown gps state " .. gps_state)
        end

        i = i + 1
    end

    if bit32.band(flags, 0x40000) > 0 then
        local lon = payload(i, 4):le_int() / 3000000.0
        local lat = payload(i + 4, 4):le_int() / 3000000.0
        -- For the speed
        -- int | band
        -- 15  | 5.58 km/h
        -- 20  | 7.38 km/h
        -- 35  | 12.78 km/h
        -- 52  | 18.90 km/h
        local speed = (payload(i + 8, 4):le_int() / 10.0) * 3.6
        local altitude = payload(i + 12, 4):le_int() / 100.0
        local timestamp = payload(i + 16, 8):le_int64()

        -- TODO: ff ff ff ff always after timestamp?
        expected_value(subtree, payload, i + 24, 0xff)
        expected_value(subtree, payload, i + 25, 0xff)
        expected_value(subtree, payload, i + 26, 0xff)
        expected_value(subtree, payload, i + 27, 0xff)

        local bearing = payload(i + 28, 2):le_int() -- Not sure?

        expected_zero(subtree, payload, i + 30)

        subtree:add(payload(i, 4), "Lon: " .. lon)
        subtree:add(payload(i + 4, 4), "Lat: " .. lat)
        subtree:add(payload(i + 8, 4), "Speed: " .. speed .. " km/h")
        subtree:add(payload(i + 12, 4), "Altitude: " .. altitude)
        subtree:add(payload(i + 16, 8), "Timestamp: " .. os.date("%Y/%m/%d %H:%M:%S", (timestamp / 1000):tonumber()))
        subtree:add(payload(i + 28, 2), "Bearing?: " .. bearing) -- Not sure

        i = i + 31
    end

    if i ~= payload:len() then
        on_error(subtree, "Unexpected payload length " .. payload:len())
    end
end

function parse_handle_chunked(payload, pinfo, subtree)
    if payload(0, 1):uint() == 0x00 then
        local chunk_flags = payload(1, 1):uint()

        if bit32.band(chunk_flags, 0x80) == 0 or bit32.band(chunk_flags, 0x40) == 0 then
            -- The chunk got split and we don't yet handle that
            on_error(subtree, "Split chunked commands are not supported")
            return
        end

        expected_zero(subtree, payload, 2)

        local chunk_command = payload(3, 1):uint()

        if chunk_command == 0x1e then
            pinfo.cols.info = 'DISPLAY_ITEMS'
            parse_display_items(payload, subtree)
        elseif chunk_command == 0x0b and bit32.band(chunk_flags, 0x09) > 0 then
            pinfo.cols.info = 'WORKOUT_TYPES'
            parse_workout_types(payload, subtree)
        elseif chunk_command == 0x0b and bit32.band(chunk_flags, 0x02) > 0 then
            pinfo.cols.info = 'SET_REMINDER'
            on_warn(subtree, "TODO: Parse reminders")
        elseif chunk_command == 0x40 then
            pinfo.cols.info = 'PHONE_VOLUME'
            parse_phone_volume(payload, subtree)
        elseif chunk_command == 0x20 then
            pinfo.cols.info = 'VIBRATION_PATTERN'
            parse_vibration(payload, subtree)
        elseif chunk_command == 0x06 then
            pinfo.cols.info = 'GPS_LOCATION'
            parse_gps_location(payload(4, payload:len() - 4), subtree)
        elseif workout_status_types[chunk_command] ~= nil then
            pinfo.cols.info = 'WORKOUT_COMMAND_FROM_PHONE'
            status_type = workout_status_types[chunk_command]

            subtree:add(payload(1, 1), "Workout Status: " .. status_type)
            on_warn(subtree, "WIP experiental")
        else
            on_warn(subtree, "Unhandled chunk")
        end
    elseif payload(0, 1):uint() == 0x10 then
        pinfo.cols.info = 'CHUNKED_RESPONSE'

        on_warn(subtree, "Unhandled chunk response")

        -- response
    end
end

--
-- Heart Rate
--

function parse_handle_hr_control_point(payload, pinfo, subtree)
    local point = payload(0, 1):uint()
    if point == 0x14 then
        pinfo.cols.info = "SET_PERIODIC_HR_MEASUREMENT_INTERVAL"

        subtree:add(payload(0, 1), "HR Control Point: COMMAND_SET_PERIODIC_HR_MEASUREMENT_INTERVAL")
        subtree:add(payload(1, 1), "Interval: " .. payload(1, 1):uint() .. " minutes")

        expected_length(subtree, payload, 2)
    elseif point == 0x15 then
        pinfo.cols.info = "HR_SLEEP_MEASUREMENT"

        subtree:add(payload(0, 1), "HR Control Point: HR_SLEEP_MEASUREMENT")
        subtree:add(payload(2, 1), "Enabled: " .. to_bool(payload(2, 1):uint(), subtree))

        expected_zero(subtree, payload, 1)
        expected_length(subtree, payload, 3)
    else
        on_error(subtree, "Unknown hr control point " .. point)
    end
end

function parse_handle_hr_measurement(payload, pinfo, subtree)
    expected_length(subtree, payload, 2)
    local val = payload(1, 1):uint()
    pinfo.cols.info = "HR_MEASUREMENT"
    subtree:add(payload(1, 1), "Value: " .. val .. " bpm")
end

--
-- User Settings
--

function parse_user_settings(payload, pinfo, subtree)
    local setting_code = payload(0, 1):uint()

    if setting_code == 0x20 then
        pinfo.cols.info = "BAND_WRIST"

        if payload(3, 1):uint() == 0x02 then
            subtree:add(payload(3, 1), "Band Wrist: left")
        elseif payload(3, 1):uint() == 0x82 then
            subtree:add(payload(3, 1), "Band Wrist: right")
        else
            on_error(subtree, "Unexpected wrist " .. payload(3, 1):uint())
        end

        expected_zero(subtree, payload, 1)
        expected_zero(subtree, payload, 2)
        expected_length(subtree, payload, 4)
    else
        on_error(subtree, "Unknown user setting " .. setting_code)
    end
end

--
-- Device Events
--

workout_types = {
    [0x01] = "Outdoor running",
    [0x04] = "Walking",
    [0x02] = "Treadmill",
    [0x03] = "Outdoor cycling",
    [0x09] = "Indoor cycling",
    [0x05] = "Pool swimming",
    [0x0b] = "Freestyle",
    [0x06] = "Elliptical",
    [0x08] = "Jump rope",
    [0x07] = "Rowing machine",
    [0x0a] = "Yoga",
}

function parse_device_event(payload, pinfo, subtree)
    local event_code = payload(0, 1):uint()

    if event_code == 0x14 then
        pinfo.cols.info = "WORKOUT_OPENED"

        workout_type = workout_types[payload(3, 1):uint()]

        subtree:add(payload(3, 1), "Workout Type: " .. workout_type)
        subtree:add(payload(2, 1), "GPS: " .. to_bool(payload(2, 1):uint(), subtree))

        expected_zero(subtree, payload, 1)
        expected_length(subtree, payload, 4)
    elseif event_code == 0x10 then
        pinfo.cols.info = "SILENT_MODE_FROM_BAND"
        expected_length(subtree, payload, 2)
        subtree:add(payload(1, 1), "Silent Mode: " .. to_bool(payload(1, 1):uint(), subtree))
    else
        on_error(subtree, "Unknown device event " .. event_code)
    end
end

function parse_realtime_steps(payload, pinfo, subtree)
    expected_length(subtree, payload, 13)

    -- TODO: what are the rest of the bytes? mostly zeroes

    subtree:add(payload(1, 2), "Number of steps: " .. payload(1, 2):le_uint())
end

workout_status_types = {
    [0x02] = "Start",
    [0x03] = "Pause",
    [0x04] = "Resume",
    [0x05] = "End",
}

function parse_workout_state(payload, pinfo, subtree)
    if payload(0, 1):uint() == 0x01 then
        pinfo.cols.info = "WORKOUT_START_FROM_PHONE"
        expected_length(subtree, payload, 3)
        workout_type = workout_types[payload(2, 1):uint()]
        subtree:add(payload(1, 1), "Workout Type: " .. workout_type)

        -- TODO payload(1, 0)
    elseif payload(0, 1):uint() == 0x11 then
        pinfo.cols.info = "WORKOUT_STATUS"

        expected_length(subtree, payload, 2)

        status_type = workout_status_types[payload(1, 1):uint()]

        subtree:add(payload(1, 1), "Workout Status: " .. status_type)
    elseif payload(0, 1):uint() == 0x10 then
        pinfo.cols.info = "WORKOUT_STATUS_ACK"

        expected_length(subtree, payload, 3)

        status_type = workout_status_types[payload(1, 1):uint()]

        subtree:add(payload(1, 1), "Workout Status: " .. status_type)
        subtree:add(payload(2, 1), "Success: " .. to_bool(payload(2, 1):uint(), subtree))
    else
        on_error(subtree, "Unknown Workout State " .. payload(0, 1):uint())
    end
end

--
-- Handles
--

HANDLES = {
    -- [UUID: Heart Rate Control Point (0x2a39)]
    [0x002c] = { "HANDLE_HR_CONTROL_POINT", parse_handle_hr_control_point },

    -- [UUID: Heart Rate Measurement (0x2a37)]
    [0x0029] = { "HANDLE_HR_MEASUREMENT", parse_handle_hr_measurement },

    -- [UUID: 000000200000351221180009af100700]
    [0x0050] = { "UUID_CHARACTERISTIC_CHUNKEDTRANSFER", parse_handle_chunked },

    -- [UUID: 000000030000351221180009af100700]
    [0x0038] = { "UUID_CHARACTERISTIC_3_CONFIGURATION", parse_handle_configuration },

    -- [UUID: 000000160000351221180009af100700]
    [0x0062] = { "UUID_CHARACTERISTIC_CHUNKEDTRANSFER_2021_WRITE", parse_handle_noop },

    -- [UUID: 000000010000351221180009af100700]
    [0x0032] = { "UUID_UNKNOWN_CHARACTERISTIC1", parse_handle_noop },

    -- [UUID: 000000020000351221180009af100700]
    [0x0035] = { "UUID_UNKNOWN_CHARACTERISTIC2", parse_handle_noop },

    -- [UUID: 000000040000351221180009af100700]
    [0x003e] = { "UUID_UNKNOWN_CHARACTERISTIC4", parse_handle_noop },

    -- [UUID: 000000140000351221180009af100700]
    [0x0015] = { "UUID_UNKNOWN_CHARACTERISTIC5", parse_handle_noop },

    -- [UUID: 000000090000351221180009af100700]
    [0x0069] = { "UUID_CHARACTERISTIC_AUTH", parse_handle_noop },

    -- [UUID: 000000080000351221180009af100700]
    [0x004a] = { "UUID_CHARACTERISTIC_8_USER_SETTINGS", parse_user_settings },

    -- [UUID: 000000100000351221180009af100700]
    [0x004d] = { "UUID_CHARACTERISTIC_DEVICEEVENT", parse_device_event },

    -- [UUID: 000000100000351221180009af100700]
    [0x0047] = { "UUID_CHARACTERISTIC_7_REALTIME_STEPS", parse_realtime_steps },

    -- [UUID: 000000060000351221180009af100700]
    [0x0044] = { "UUID_CHARACTERISTIC_6_BATTERY_INFO", parse_handle_noop },

    -- [UUID: 000000050000351221180009af100700]
    -- [Characteristic UUID: 000000050000351221180009af100700]
    [0x0041] = { "UUID_CHARACTERISTIC_5_ACTIVITY_DATA", parse_handle_noop },
    [0x0042] = { "UUID_CHARACTERISTIC_5_ACTIVITY_DATA", parse_handle_noop },

    -- [UUID: 000000170000351221180009af100700]
    [0x0065] = { "UUID_CHARACTERISTIC_CHUNKEDTRANSFER_2021_READ", parse_handle_noop },

    -- [UUID: 0000000f0000351221180009af100700]
    [0x0056] = { "UUID_CHARACTERISTIC_WORKOUTSTATE", parse_workout_state },
}

-- Get the btatt handle of the current packet
function get_handle()
    local fields = { all_field_infos() }

    for ix, finfo in ipairs(fields) do
        if finfo.name == 'btatt' then
            return fields[ix].range(1, 1):uint()
        end
    end

    return nil
end

--
-- Dissector base
--

function mi_proto.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol = mi_proto.name

    local subtree_data = tree:add(mi_proto, buffer, "Huami Data")

    pinfo.cols.info = HANDLES[get_handle()][1]
    HANDLES[get_handle()][2](buffer, pinfo, subtree_data)
end

local btatt_handle = DissectorTable.get("btatt.handle")
for handle, _ in pairs(HANDLES) do
    btatt_handle:add(handle, mi_proto)
end
