<?php
/*  Copyright (C) 2021 Andreas Shimokawa

    This file is part of Gadgetbridge-tools.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
require("typemap.php");

if ($argc < 3) {
    echo "USAGE: " . $argv[0] . " <directory> <outputfile>\n";
    exit;
}

$inputdir = $argv[1];
$outputfile = $argv[2];

$content = "";

foreach ($typemap as $type => $inputfilename) {
    $filecontent = @file_get_contents("$inputdir/$inputfilename");
    if (!$filecontent) {
        echo "[E] File not found: $inputfilename\n";
        exit;
    }
    echo "[I] Packing $inputfilename\n";
    // prepend file header
    $fileheader = chr(1) . chr($type) . encode_uint32(strlen($filecontent)) . encode_uint32(crc32($filecontent));
    $content .= $fileheader . $filecontent;
}

echo "[I] Adding header\n";
$header = "UIHH"  . chr(0x04) . chr(0x00) . chr(0x00) . chr(0x00)
    . chr(0x00) . chr(0x00) . chr(0x00) . chr(0x01) . encode_uint32(crc32($content))
    . chr(0x00) . chr(0x00) . chr(0x00) . chr(0x00) . chr(0x00) . chr(0x00) . encode_uint32(strlen($content))
    . chr(0x00) . chr(0x00) . chr(0x00) . chr(0x00) . chr(0x00) . chr(0x00);

$content = $header . $content;

echo "[I] Writing $outputfile\n";

file_put_contents($outputfile, $content);

function encode_uint32($value)
{
    return chr($value & 0xff) . chr(($value >> 8) & 0xff) . chr(($value >> 16) & 0xff) . chr(($value >> 24) & 0xff);
}

echo "[I] All done.\n";
