console.log("I managed to use Frida!");

log("load script")
Java.perform(function() {
    log("load java")
    var clzlst = []
    Java.enumerateLoadedClasses({
        onMatch: function(clzname) {
            clzlst.push(clzname)
        },
        onComplete: function(e) {
            console.log("<<<<onComplete");
        }
    });
});

function log(msg) {
    console.log(msg)
}

  var hook = Java.use('xfkj.fitpro.bluetooth.ReceiveData');
  var method = hook.Sport;
  method.implementation = function (str1) {
        console.log('SPORTS: ', str1);
        method.call(this, str1);
  };

  var model = Java.use('xfkj.fitpro.application.MyApplication');
  var sdm = model.Logdebug;
  sdm.implementation = function (str1,str2) {
        console.log('DEBUG', str1,str2);
        sdm.call(this, str1,str2);
  };
